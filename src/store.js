
import { reactive } from 'vue'

export const store = reactive({
    space: ['','','','','','','','',''],
    setSpace(updatedSpace,i) {
       return  this.space[i] = updatedSpace
        // console.log("updatedSpace",updatedSpace)
    },

    currentGameStatus:[],
    setCurrentGameStatus(currentstatus){
       return  this.currentGameStatus = currentstatus
    },

    lastPlay:'',
    setLastPlay(updatedSpace){
        return this.lastPlay = updatedSpace
    },

    winner:'',
    setWinner(getwinner){
       return  this.winner = getwinner
    }
    
})